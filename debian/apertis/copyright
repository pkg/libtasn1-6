Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: no-info-found
License: GFDL or GPL-3+ or LGPL-2.1+

Files: AUTHORS
 NEWS
 THANKS
Copyright: 2002-2025, Free Software Foundation, Inc.
License: FSFAP

Files: GNUmakefile
 Makefile.am
 Makefile.in
 cfg.mk
 configure.ac
 maint.mk
Copyright: 1985, 1986, 1988, 1990-2025, Free Software Foundation, Inc.
License: GPL-3+

Files: INSTALL
Copyright: 1994-1996, 1999-2002, 2004-2017, 2020-2024, Free Software
License: FSFAP

Files: aclocal.m4
Copyright: 1992-2025, Free Software Foundation, Inc.
License: FSFULLR

Files: build-aux/*
Copyright: 1985, 1986, 1988, 1990-2025, Free Software Foundation, Inc.
License: GPL-3+

Files: build-aux/compile
 build-aux/depcomp
 build-aux/mdate-sh
 build-aux/missing
 build-aux/test-driver
 build-aux/ylwrap
Copyright: 1995-2025, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: build-aux/gnupload
Copyright: 2004-2025, Free Software Foundation, Inc.
License: GPL-2+

Files: build-aux/install-sh
Copyright: 1994, X Consortium
License: X11

Files: build-aux/ltmain.sh
Copyright: 1996-2019, 2021, 2022, Free Software Foundation, Inc.
License: (Expat or GPL-2+) with Libtool exception

Files: configure
Copyright: 1992-1996, 1998-2017, 2020, 2021, Free Software Foundation
License: FSFUL

Files: doc/*
Copyright: no-info-found
License: GFDL-1.3

Files: doc/Makefile.am
 doc/Makefile.in
Copyright: 1985, 1986, 1988, 1990-2025, Free Software Foundation, Inc.
License: GPL-3+

Files: doc/TODO
Copyright: 2002-2025, Free Software Foundation, Inc.
License: FSFAP

Files: doc/asn1Coding.1
 doc/asn1Decoding.1
 doc/asn1Parser.1
Copyright: Copyright (co 2025, Free Software Foundation, Inc.
License: GFDL-1.3

Files: doc/fdl-1.3.texi
Copyright: 2000, 2001, 2002, 2007, 2008, Free Software Foundation, Inc.
License: GFDL-1.3

Files: doc/gdoc
Copyright: 2013, Adam Sampson
 2002-2021, Simon Josefsson
 2001, 2002, Nikos Mavrogiannopoulos
 1998, Michael Zucchi
License: GPL-3+

Files: doc/libtasn1.info
 doc/libtasn1.texi
Copyright: 2001-2025, Free Software Foundation, Inc.
License: GFDL-1.3+ or GPL-3+ or LGPL-2.1+

Files: doc/man/*
Copyright: Copyright (co 2006-2025, Free Software Foundation, Inc..
License: FSFAP

Files: doc/reference/Makefile.am
Copyright: 2007-2017, Stefan Sauer
License: GPL-3+

Files: doc/reference/Makefile.in
Copyright: 1985, 1986, 1988, 1990-2025, Free Software Foundation, Inc.
License: GPL-3+

Files: examples/*
Copyright: 1985, 1986, 1988, 1990-2025, Free Software Foundation, Inc.
License: GPL-3+

Files: fuzz/*
Copyright: 1999-2025, Free Software Foundation, Inc.
License: LGPL-3+

Files: fuzz/Makefile.am
 fuzz/main.c
Copyright: 2017-2019, Tim Ruehsen
License: Expat

Files: fuzz/Makefile.in
Copyright: 1994-2021, Free Software Foundation, Inc.
License: Expat

Files: fuzz/corpus2array.c
Copyright: 1987-2025, Free Software Foundation, Inc.
License: LGPL-2.1+

Files: gtk-doc.make
Copyright: 2003, James Henstridge
License: GPL-3+

Files: lib/*
Copyright: 1987-2025, Free Software Foundation, Inc.
License: LGPL-2.1+

Files: lib/ASN1.c
Copyright: 1984, 1989, 1990, 2000-2015, 2018-2021, Free Software Foundation
License: (GPL-3+ or LGPL-2.1+) with Bison-2.2 exception

Files: lib/Makefile.am
 lib/Makefile.in
Copyright: 1985, 1986, 1988, 1990-2025, Free Software Foundation, Inc.
License: GPL-3+

Files: lib/gl/Makefile.am
 lib/gl/Makefile.in
Copyright: 1985, 1986, 1988, 1990-2025, Free Software Foundation, Inc.
License: GPL-3+

Files: lib/gl/_Noreturn.h
 lib/gl/arg-nonnull.h
 lib/gl/c++defs.h
 lib/gl/warn-on-use.h
Copyright: 2009-2025, Free Software Foundation, Inc.
License: LGPL-2+

Files: lib/gl/cdefs.h
Copyright: The GNU Toolchain Authors.
 1992-2025, Free Software Foundation, Inc.
License: LGPL-2.1+

Files: lib/gl/minmax.h
Copyright: 1992, 1995, 1997-1999, 2001, 2003-2007, 2009-2025, Free Software
License: LGPL-2.1+

Files: m4/*
Copyright: 1992-2025, Free Software Foundation, Inc.
License: FSFULLR

Files: m4/ax_ac_append_to_file.m4
 m4/ax_ac_print_to_file.m4
Copyright: 2009, Allan Caffee <allan.caffee@gmail.com>
License: FSFAP

Files: m4/ax_add_am_macro_static.m4
 m4/ax_am_macros_static.m4
Copyright: 2009, Tom Howard <tomhoward@users.sf.net>
 2009, Allan Caffee <allan.caffee@gmail.com>
License: FSFAP

Files: m4/ax_check_gnu_make.m4
Copyright: 2015, Enrico M. Crisostomo <enrico.m.crisostomo@gmail.com>
 2008, John Darrington <j.darrington@elvis.murdoch.edu.au>
License: FSFAP

Files: m4/ax_code_coverage.m4
Copyright: 2015, 2018, Bastien ROUCARIES
 2012, Xan Lopez
 2012, Paolo Borelli
 2012, Dan Winship
 2012, Christian Persch
 2012, 2016, Philip Withnall
License: LGPL-2.1+

Files: m4/ax_file_escapes.m4
Copyright: 2008, Tom Howard <tomhoward@users.sf.net>
License: FSFAP

Files: m4/codeset.m4
 m4/ltsugar.m4
Copyright: 1998-2025, Free Software
License: FSFULLR

Files: m4/gnulib-comp.m4
Copyright: 1985, 1986, 1988, 1990-2025, Free Software Foundation, Inc.
License: GPL-3+

Files: m4/gtk-doc.m4
Copyright: 2003, James Henstridge
License: GPL-3+

Files: m4/libtool.m4
Copyright: 1996-2001, 2003-2019, 2021, 2022, Free Software
License: (FSFULLR or GPL-2) with Libtool exception

Files: m4/ltoptions.m4
 m4/lt~obsolete.m4
Copyright: 2004, 2005, 2007-2009, 2011-2019, 2021, 2022, Free
License: FSFULLR

Files: m4/ltversion.m4
Copyright: 2000-2007, 2009-2025, Free Software Foundation
License: FSFULLR

Files: m4/pkg.m4
Copyright: 2004, Scott James Remnant <scott@netsplit.com>.
License: GPL-2+ with Autoconf-data exception

Files: src/*
Copyright: 1987-2025, Free Software Foundation, Inc.
License: LGPL-2.1+

Files: src/Makefile.am
 src/Makefile.in
 src/asn1Coding.c
 src/asn1Decoding.c
 src/asn1Parser.c
 src/benchmark.c
 src/benchmark.h
Copyright: 1985, 1986, 1988, 1990-2025, Free Software Foundation, Inc.
License: GPL-3+

Files: src/gl/Makefile.am
 src/gl/Makefile.in
 src/gl/progname.c
 src/gl/progname.h
Copyright: 1985, 1986, 1988, 1990-2025, Free Software Foundation, Inc.
License: GPL-3+

Files: src/gl/_Noreturn.h
 src/gl/arg-nonnull.h
 src/gl/c++defs.h
 src/gl/warn-on-use.h
Copyright: 2009-2025, Free Software Foundation, Inc.
License: LGPL-2+

Files: src/gl/alloca.in.h
Copyright: 1995, 1999, 2001-2004, 2006-2025, Free Software Foundation
License: LGPL-2.1+

Files: src/gl/gettime.c
 src/gl/timespec.c
 src/gl/version-etc-fsf.c
 src/gl/version-etc.c
 src/gl/version-etc.h
Copyright: 1999-2025, Free Software Foundation, Inc.
License: LGPL-3+

Files: src/gl/m4/*
Copyright: 1992-2025, Free Software Foundation, Inc.
License: FSFULLR

Files: src/gl/m4/alloca.m4
 src/gl/m4/gettimeofday.m4
 src/gl/m4/malloca.m4
 src/gl/m4/pathmax.m4
 src/gl/m4/time_h.m4
 src/gl/m4/timespec.m4
Copyright: 2000-2007, 2009-2025, Free Software Foundation
License: FSFULLR

Files: src/gl/m4/gnulib-cache.m4
 src/gl/m4/gnulib-comp.m4
Copyright: 1985, 1986, 1988, 1990-2025, Free Software Foundation, Inc.
License: GPL-3+

Files: src/gl/m4/stat-time.m4
Copyright: 1998-2025, Free Software
License: FSFULLR

Files: src/gl/pathmax.h
 src/gl/realloc.c
Copyright: 1992, 1995, 1997-1999, 2001, 2003-2007, 2009-2025, Free Software
License: LGPL-2.1+

Files: src/gl/timespec.h
Copyright: 2000, 2002, 2004, 2005, 2007, 2009-2025, Free Software
License: LGPL-3+

Files: tests/*
Copyright: no-info-found
License: GPL-3+

Files: tests/CVE-2018-1000654.c
 tests/Makefile.am
 tests/Makefile.in
 tests/Test_choice.c
 tests/Test_choice_ocsp.c
 tests/Test_encdec.c
 tests/Test_encoding.asn
 tests/Test_encoding.c
 tests/Test_errors.c
 tests/Test_indefinite.c
 tests/Test_overflow.c
 tests/Test_parser.asn
 tests/Test_parser.c
 tests/Test_simple.c
 tests/Test_strings.c
 tests/Test_tree.asn
 tests/Test_tree.c
 tests/benchmark.sh
 tests/coding-decoding2.c
 tests/coding-long-oid.c
 tests/coding.sh
 tests/copynode.c
 tests/crlf.sh
 tests/decoding-invalid-pkcs7.sh
 tests/decoding-invalid-x509.sh
 tests/decoding.sh
 tests/octet-string.c
 tests/pkix.asn
 tests/reproducers.c
 tests/setof.c
 tests/strict-der.c
 tests/threadsafety.sh
 tests/version.c
Copyright: 1985, 1986, 1988, 1990-2025, Free Software Foundation, Inc.
License: GPL-3+

Files: tests/TestIndef.p12
Copyright: no-info-found
License: GPL-3+

Files: tests/TestIndef2.p12
Copyright: no-info-found
License: GPL-3+

Files: tests/TestIndef3.der
Copyright: no-info-found
License: GPL-3+

Files: tests/crlf.cer
Copyright: no-info-found
License: GPL-3+

Files: tests/object-id-decoding.c
 tests/ocsp-basic-response.c
 tests/spc_pe_image_data.c
Copyright: 2016, Red Hat, Inc.
License: GPL-3+

Files: tests/object-id-encoding.c
Copyright: 2019, Nikos Mavrogiannopoulos
License: GPL-3+

Files: tests/ocsp-basic-response.der
Copyright: no-info-found
License: GPL-3+

Files: tests/parser.sh
Copyright: 2021-2025, Free Software Foundation, Inc.
 2019, Red Hat, Inc.
License: GPL-3+

Files: windows/*
Copyright: no-info-found
License: GPL-3+

Files: windows/libtasn14win.mk
Copyright: 1985, 1986, 1988, 1990-2025, Free Software Foundation, Inc.
License: GPL-3+
